package babylon.holczhauser.com.babylonhealth.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import babylon.holczhauser.com.babylonhealth.R;
import babylon.holczhauser.com.babylonhealth.backend.BackendImpl;
import babylon.holczhauser.com.babylonhealth.backend.dao.user.User;
import babylon.holczhauser.com.babylonhealth.ui.OnPostClickListener;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsFragment extends Fragment implements Response.Listener<User[]>, Response.ErrorListener {

    private long postID;
    private long userID;
    private String titleString;
    private String bodyString;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.imageview_avatar)
    ImageView avatarImageView;

    @Bind(R.id.title)
    TextView titleTextView;

    @Bind(R.id.body)
    TextView bodyTextView;

    @Bind(R.id.username)
    TextView userNameTextView;

    //    @Bind(R.id.number_of_comments)
//    TextView numberOfCommentsTextView;
    private boolean isUserData;
    private User user;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = View.inflate(getActivity(), R.layout.details_fragment, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        startDownloading();

        postID = getArguments().getLong("key_post_id", getResources().getInteger(R.integer.no_value));
        userID = getArguments().getLong("key_user_id", userID = getArguments().getLong("key_user_id", 0));

        titleString = getArguments().getString("key_title_id", getString(R.string.no_title));
        titleTextView.setText(titleString);

        bodyString = getArguments().getString("key_body_id", getString(R.string.no_body));
        bodyTextView.setText(bodyString);
    }

    private void startDownloading() {
        swipeRefreshLayout.setRefreshing(true);
        BackendImpl.getInstance().getUserData(getActivity().getApplicationContext(), this, this);
    }


    @Override
    public void onErrorResponse(VolleyError volleyError) {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResponse(User[] responseDao) {
        swipeRefreshLayout.setRefreshing(false);
        processUserData(responseDao);
    }

    //display the user data on the UI based on the userID
    private void processUserData(User[] userArray) {

        if (userArray != null) {
            for (User user : userArray) {
                if (user.getId() == userID) {
                    this.user = user;
                    userNameTextView.setText(user.getName());
                    BackendImpl.getInstance().downloadImage(avatarImageView, user.getEmail());
                    break;
                }
            }
        }
    }

    @OnClick(R.id.username)
    public void showUserOnMap() {
        if (user != null) {
            ((OnPostClickListener) getActivity()).showUserOnMap(user);
        } else {
            Toast.makeText(getActivity(), getString(R.string.user_data_error), Toast.LENGTH_SHORT).show();
            Log.e("Error", "user was NULL");
        }

    }

}

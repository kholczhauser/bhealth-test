package babylon.holczhauser.com.babylonhealth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.vincentbrison.openlibraries.android.dualcache.lib.DualCacheContextUtils;

import babylon.holczhauser.com.babylonhealth.R;
import babylon.holczhauser.com.babylonhealth.backend.dao.Post;
import babylon.holczhauser.com.babylonhealth.backend.dao.user.User;
import babylon.holczhauser.com.babylonhealth.ui.fragment.DetailsFragment;
import babylon.holczhauser.com.babylonhealth.ui.fragment.UserDataMapActivity;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnPostClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUp();
    }

    private void setUp() {
        //setting up database
        DualCacheContextUtils.setContext(getApplicationContext());

        //Setting up butterkinfe
        ButterKnife.bind(this);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(2 * 1024 * 1024))
                .memoryCacheSize(2 * 1024 * 1024)
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .build();
        ImageLoader.getInstance().init(config);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPostClick(Post post) {
        DetailsFragment detailsFragment = new DetailsFragment();

        Bundle args = new Bundle();
        args.putLong(KEY_POST_ID, post.getId());
        args.putLong(KEY_USER_ID, post.getUserId());
        args.putString(KEY_TITLE, post.getTitle());
        args.putString(KEY_BODY, post.getBody());
        detailsFragment.setArguments(args);


        getSupportFragmentManager().beginTransaction().addToBackStack(getString(R.string.tag_details)).replace(R.id.fragment_container, detailsFragment).commit();
    }

    @Override
    public void showUserOnMap(User user) {
        //start fragment 3 with mapview
        Intent startMap = new Intent(this, UserDataMapActivity.class);

        startMap.putExtra(UserDataMapActivity.LATITUDE_KEY, user.getAddress().getGeo().getLat());
        startMap.putExtra(UserDataMapActivity.LONGITUDE_KEY, user.getAddress().getGeo().getLng());

        startActivity(startMap);
    }
}

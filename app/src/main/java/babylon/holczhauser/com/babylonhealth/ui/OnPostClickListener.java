package babylon.holczhauser.com.babylonhealth.ui;

import babylon.holczhauser.com.babylonhealth.backend.dao.Post;
import babylon.holczhauser.com.babylonhealth.backend.dao.user.User;

/**
 * Created by Karoly Holczhauser on 3/18/2016.
 */
public interface OnPostClickListener {

    String KEY_POST_ID = "key_post_id";
    String KEY_USER_ID = "key_user_id";
    String KEY_TITLE = "key_title_id";
    String KEY_BODY = "key_body_id";

    void onPostClick(Post post);

    void showUserOnMap(User user);
}

package babylon.holczhauser.com.babylonhealth.ui.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import babylon.holczhauser.com.babylonhealth.R;
import babylon.holczhauser.com.babylonhealth.backend.BackendImpl;
import babylon.holczhauser.com.babylonhealth.backend.dao.Post;
import babylon.holczhauser.com.babylonhealth.ui.OnPostClickListener;
import babylon.holczhauser.com.babylonhealth.ui.TitleAdapter;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment implements Response.Listener<Post[]>,
        Response.ErrorListener, SwipeRefreshLayout.OnRefreshListener {

    @Bind(R.id.title_recycler_view)
    RecyclerView titleList;


    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    private OnPostClickListener onItemTapListener;

    public MainActivityFragment() {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        onItemTapListener = (OnPostClickListener) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        startDownload();
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    private void startDownload() {
        swipeRefreshLayout.setRefreshing(true);

        Toast.makeText(getContext(), R.string.download_in_progress, Toast.LENGTH_SHORT).show();

        BackendImpl.getInstance().getPosts(getActivity().getApplicationContext(), this, this);
    }

    @Override
    public void onResponse(Post[] post) {
        swipeRefreshLayout.setRefreshing(false);


        Toast.makeText(getActivity(), getText(R.string.download_ok), Toast.LENGTH_SHORT).show();

        if (post != null && post.length > 0) {
            TitleAdapter adapter = new TitleAdapter(post, onItemTapListener);
            titleList.setLayoutManager(new LinearLayoutManager(getActivity()));
            titleList.setAdapter(adapter);
            titleList.invalidate();
        }
    }

    @Override
    public void onErrorResponse(VolleyError volleyError) {
        swipeRefreshLayout.setRefreshing(false);
        Toast.makeText(getActivity(), getText(R.string.download_error), Toast.LENGTH_SHORT).show();

        Log.e("onErrorResponse", "onErrorResponse");
    }


    @Override
    public void onRefresh() {
        BackendImpl.getInstance().getPosts(getActivity().getApplicationContext(), this, this);
    }
}

package babylon.holczhauser.com.babylonhealth.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import babylon.holczhauser.com.babylonhealth.R;
import babylon.holczhauser.com.babylonhealth.backend.dao.Post;

public class TitleAdapter extends RecyclerView.Adapter<TitleAdapter.ViewHolder> {
    private final Post[] dataArray;
    private final OnPostClickListener onItemTap;

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView titleTextView;
        private RelativeLayout container;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.textview_title);
            container = (RelativeLayout) itemView.findViewById(R.id.row_container);
            //    image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public TitleAdapter(Post[] post,OnPostClickListener  onItemTapListener) {
        this.dataArray = post;
        this.onItemTap = onItemTapListener;
    }

    @Override
    public TitleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.title_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(TitleAdapter.ViewHolder holder, final int position) {
        Post item = dataArray[position];
        holder.titleTextView.setText(item.getTitle());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemTap.onPostClick(dataArray[position]);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (dataArray != null) {
            return dataArray.length;
        }
        return 0;
    }
}

package babylon.holczhauser.com.babylonhealth.backend;

import android.content.Context;
import android.widget.ImageView;

import com.android.volley.Response;

import babylon.holczhauser.com.babylonhealth.backend.dao.Post;
import babylon.holczhauser.com.babylonhealth.backend.dao.user.User;

/**
 * Created by Karoly Holczhauser on 3/17/2016.
 */
public interface BackendInterface {

    void getPosts(Context c, Response.Listener<Post[]> successListener, Response.ErrorListener errorListener);

    void getComments();

    void getUserData(Context c, Response.Listener<User[]> successListener, Response.ErrorListener errorListener);

    void downloadImage(ImageView iw, String email);

}

package babylon.holczhauser.com.babylonhealth.backend;

import android.content.Context;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.HashMap;

import babylon.holczhauser.com.babylonhealth.backend.dao.Post;
import babylon.holczhauser.com.babylonhealth.backend.dao.user.User;

//Using enum to have a thread safe Singleton
public class BackendImpl implements BackendInterface{
    private static enum Singleton { // private, why not
        INSTANCE;
        private static final BackendImpl singleton = new BackendImpl();

        public BackendImpl getSingleton() {
            return singleton;
        }
    }
    private BackendImpl(){}

    public static BackendImpl getInstance() {
        return BackendImpl.Singleton.INSTANCE.getSingleton();
    }


    private static final String URL_POSTS = "http://jsonplaceholder.typicode.com/posts";
    private static final String URL_COMMENTS = "http://jsonplaceholder.typicode.com/comments";
    private static final String URL_USER_DATA = "http://jsonplaceholder.typicode.com/users";
    private static final String AVATAR_BASE_URL = "https://api.adorable.io/avatars/186/";



    @Override
    public void getPosts(Context c, Response.Listener<Post[]> successListener, Response.ErrorListener errorListener) {
        HashMap<String,String> header = new HashMap<>();
        header.put("Content-Type","text/json");

        GsonRequest<Post[]> postGsonRequest = new GsonRequest<>(
                URL_POSTS, Post[].class, header, successListener, errorListener);
        Volley.newRequestQueue(c).add(postGsonRequest);
    }

    @Override
    public void getComments() {

    }

    @Override
    public void getUserData(Context c, Response.Listener<User[]> successListener, Response.ErrorListener errorListener) {
        HashMap<String,String> header = new HashMap<>();
        header.put("Content-Type","text/json");

        GsonRequest<User[]> postGsonRequest = new GsonRequest<>(
                URL_USER_DATA, User[].class, header, successListener, errorListener);
        Volley.newRequestQueue(c).add(postGsonRequest);
    }

    @Override
    public void downloadImage(ImageView iw, String email) {
        ImageLoader.getInstance().displayImage(AVATAR_BASE_URL+email , iw);
    }
}
